module VGO

go 1.13

require (
	github.com/aliyun/alibaba-cloud-sdk-go v0.0.0-20190808125512-07798873deee
	github.com/aliyun/aliyun-oss-go-sdk v2.0.1+incompatible
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-gonic/gin v1.4.0
	github.com/gorilla/websocket v1.4.1
	github.com/inconshreveable/go-update v0.0.0-20160112193335-8152e7eb6ccf
	github.com/jinzhu/gorm v1.9.10
	github.com/stianeikeland/go-rpio/v4 v4.4.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	github.com/youpy/go-riff v0.0.0-20131220112943-557d78c11efb // indirect
	github.com/youpy/go-wav v0.0.0-20160223082350-b63a9887d320
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa
	gopkg.in/ini.v1 v1.44.0
)
